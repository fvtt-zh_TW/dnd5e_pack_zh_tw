# Foundry VTT D&D 5e 正體中文合集

## 安装方法
- 使用模组管理器貼上連結並安装 https://gitlab.com/fvtt-zh_TW/dnd5e_pack_zh_TW/-/raw/master/module.json

![image](https://gitlab.com/fvtt-zh_TW/dnd5e_pack_zh_TW/-/raw/master/image.png)

包含了職業表和法術合集，方便創角，DM 可以根據需求進行更改

## 鳴謝
**感谢其他原中文化版本譯者**
